# Radio module

The radio repository is part of the WUST Satellite project. 

The radio repository includes the schematic, PCB design, and software for Cubesat. 
The hardware was designed using KiCad software.

## LoRa radio 
![LoRa module PCB render](Docs/LoRa_radio_module.png)

Basic functions of the LoRa module:
1. Long-range communication
    - The module can communicate using a dipole antenna up to about 1.5 kilometers.
2. Uploaded software for basic communication with EPS, Raspberry Pi, and Lora ground station:
    - The module can read data from EPS and send it over radio and to Raspberry Pi.
    - The module can respond to the PING command.
    - The module has implemented its own frame for Lora communication with CRC16.


LoRa frame:

    | START (1 byte) | LENGTH (1 byte) | CMD (1 byte) | DATA (LENGTH) | CRC (2 bytes) |

Commands:

|    Command name    | Command code |
|:------------------:|:------------:|
|        PING        |     0xA1     |
| VOLTAGE_CURRENT_RX |     0xA2     |
| VOLTAGE_CURRENT_TX |     0xB1     |
|         ACK        |     0xA0     |
|        ERROR       |     0xFF     |

What is **NOT** implemented:
1. Watchdog
    - The external watchdog doesn't work properly. Probably the wrong type of external watchdog.

## Project structure
```
├── Docs
├── LoRa_radio_module
│   ├── Adds
│   │   └── **contains libraries for PCB**
│   ├── gerber
│   └── LoRa_radio_module-backups
└── Software
    ├── lora
    │   ├── App **contains files for radio communication, telemetry itp. **
    │   ├── Core
    │   │   ├── Inc
    │   │   ├── Src
    │   │   └── Startup
    │   ├── Debug
    │   │   ├── App
    │   │   ├── Core
    │   │   └── Drivers
    │   └── Drivers
    │       ├── CMSIS
    │       ├── LoRa
    │       └── STM32F4xx_HAL_Driver
    ├── LoRa_ground_station_test **Program for ground station test device**
    │   ├── App
    │   ├── Core
    │   │   ├── Inc
    │   │   ├── Src
    │   │   └── Startup
    │   ├── Debug
    │   │   ├── App
    │   │   ├── Core
    │   │   └── Drivers
    │   └── Drivers
    │       ├── CMSIS
    │       ├── LoRa
    │       └── STM32L4xx_HAL_Driver
    └── test
        ├── Core
        │   ├── Inc
        │   ├── Src
        │   └── Startup
        ├── Debug
        │   ├── Core
        │   └── Drivers
        └── Drivers
            ├── CMSIS
            └── STM32F4xx_HAL_Driver
```
# Ground station test code

Repository also contains the code for the ground station tester in the [LoRa_ground_station_test](Software/LoRa_ground_station_test/) folder.

The device includes NUCLEO-L432KC, RA-02 LoRa module and antenna. 

# Responsibility

The person who will be responsible for the repository is [Jakub Sobczyk](https://gitlab.com/sobczykjakub87).
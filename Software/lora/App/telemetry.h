#ifndef TELEMETRY_H
#define TELEMETRY_H
/*!
 * \file telemetry.h
 * \author Jakub Sobczyk (sobczykjakub87@gmail.com)
 * \brief Telemetry module
 * \version 0.1
 * \date 24-06-2024
 * 
 * @copyright MIT license
 * 
 */

#include "data.h"
#include "main.h"
#include <string.h>


/*!
 * \brief Telemetry initialization
 * 
 * Function initializes telemetry structure
 * 
 * \param telemetry pointer to telemetry structure
 */
void Telemetry_init(telemetry_t * telemetry);













#endif

#ifndef DATA_H
#define DATA_H

/*!
 * \file data.h
 * \author Jakub Sobczyk (sobczykjakub87@gmail.com)
 * \brief Documentation for data.h module
 * \version 0.1
 * \date 24-06-2024
 * 
 * @copyright MIT license 
 * 
 */

#include "main.h"

/*!
 * \brief Enum for telemetry state
 * 
 * Define the state of telemetry
 * 
 */
typedef enum
{
	T_WAITING,
	T_RECEIVE,
	T_SEND,
	T_ERROR = 127,
} state_t;

/*!
 * \brief telemetry_t structure
 *
 * Structure for telemetry data. Holds the state of telemetry, voltages and currents of ACU1 and ACU2, EPS status, last data received and data to send.
 * 
 */
typedef struct
{
	state_t _state;
	uint16_t _voltage_acu1;
	uint16_t _voltage_acu2;
	uint16_t _current_acu1;
	uint16_t _current_acu2;
	uint8_t _eps_status;
	uint8_t _last_data_received[128];
	uint8_t _data_to_send[128];
} telemetry_t;


#endif

#ifndef RADIO_PROTOCOL_H
#define RADIO_PROTOCOL_H

/*!
 * \file radio_protocol.h
 * \author Jakub Sobczyk (sobczykjakub87@gmail.com)
 * \brief Defines functions and structures for radio protocol
 * \version 0.1
 * \date 24-06-2024
 * 
 * @copyright MIT license
 * 
 */

#include "data.h"

#define CRCINITIAL 0xFFFF
#define CRCPOLYNOMIAL 0x1021

#define ID 0XAA


#define R_PING 0xA1
#define R_ACK 0x0A
#define R_ERROR 0xFF
#define R_VOLTAGE_CURRENT_RX 0xA2
#define R_VOLTAGE_CURRENT_TX 0xB1

/*!
 * \brief Header structure
 * 
 * Structure for header of radio frame. Holds the id, command and lenght of data.
 */
typedef struct
{
	uint8_t id;
	uint8_t command;
	uint8_t lenght;
} header_t;

/*!
 * \brief Payload structure
 * 
 * Structure for payload of radio frame. Holds the data and crc.
 */
typedef struct
{
	uint8_t data[120];
	uint16_t crc;

} payload_t;

/*!
 * \brief  Frame structure
 * 
 * Structure for radio frame. Holds the header and payload.
 */
typedef struct
{
	header_t header;
	payload_t payload;
} frame_t;


/*
 * Radio frame
 * | START (1 byte) | LENGHT (1 byte) | CMD (1 byte) | DATA (LENGHT) | CRC |
 */


/*!
 * \brief Read data to frame
 * 
 * Function reads data from buffer to frame
 * \param buf - pointer to buffer
 * \param rxFrame - pointer to frame
 * \retval 1 - if frame is correct
 * \retval 0 - if frame is incorrect
 */
uint8_t ReadDataToFrame(uint8_t * buf, frame_t * rxFrame);

/*!
 * \brief Parse data
 * 
 * Function parses data from rxFrame and prepares txFrame
 * \param rxFrame - pointer to received frame
 * \param txFrame - pointer to frame to send
 * \param telemetry - pointer to telemetry structure
 * \retval 1 - if frame is correct
 * \retval 0 - if frame is incorrect
*/
uint8_t ParseData(frame_t * rxFrame, frame_t * txFrame, telemetry_t * telemetry);

/*!
 * \brief Prepare frame
 * 
 * Function prepares frame to send
 * \param Frame - pointer to frame
 * \param command - command to send
 * \param data - pointer to data to send
 * \param lenght - lenght of data
 */
void PrepareFrame(frame_t * Frame, uint8_t command, uint8_t * data, uint8_t lenght);

/*!
 * \brief Convert frame
 * 
 * Function converts frame to buffer
 * \param frame - pointer to frame
 * \param buffer - pointer to buffer
 * \return lenght of buffer
 */
uint8_t ConverFrame(frame_t * frame, uint8_t * buffer);


/*!
 * \brief CRC16 calculate
 * 
 * Function calculates CRC16
 * \param buffer - pointer to buffer
 * \param length - lenght of buffer
 * \return CRC16
 */
uint16_t crc16Calculate(const uint8_t *buffer, size_t length);




#endif

#include "radio_protocol.h"


uint8_t ReadDataToFrame(uint8_t * buf, frame_t * rxFrame)
{
	uint16_t crc;
	rxFrame->header.id = buf[0];
	rxFrame->header.command = buf[1];
	rxFrame->header.lenght = buf[2];

	for (uint8_t i = 0; i < rxFrame->header.lenght; ++i)
	{
		rxFrame->payload.data[i] = buf[3+i];
	}

	rxFrame->payload.crc = ((buf[3+rxFrame->header.lenght] << 8) | buf[4+rxFrame->header.lenght]);

	crc = crc16Calculate(buf, rxFrame->header.lenght + 3);

	if (rxFrame->header.id == ID && crc == rxFrame->payload.crc)
	{
		return 1;
	}
	return 0;
}

uint8_t ParseData(frame_t * rxFrame, frame_t * txFrame, telemetry_t * telemetry)
{
	uint8_t state;
	uint8_t buf[120];
	switch (rxFrame->header.command)
	{
		case R_PING:
			PrepareFrame(txFrame, R_ACK, 0, 0);
			state = 1;
			break;
		case R_VOLTAGE_CURRENT_RX:
			buf[0] = telemetry->_voltage_acu1 >> 8;
			buf[1] = telemetry->_voltage_acu1;
			buf[2] = telemetry->_voltage_acu2 >> 8;
			buf[3] = telemetry->_voltage_acu2;
			buf[4] = telemetry->_current_acu1 >> 8;
			buf[5] = telemetry->_current_acu1;
			buf[6] = telemetry->_current_acu2 >> 8;
			buf[7] = telemetry->_current_acu2;
			PrepareFrame(txFrame, R_VOLTAGE_CURRENT_TX, buf, 8);
			state = 1;
			break;
		default:
			state = 0;
			break;
	}
	return state;
}

void PrepareFrame(frame_t * Frame, uint8_t command, uint8_t * data, uint8_t data_lenght)
{
	Frame->header.id = ID;
	Frame->header.command = command;
	Frame->header.lenght = data_lenght;

	if (data_lenght > 0)
	{
		for (uint8_t i = 0; i < data_lenght; ++i)
		{
			Frame->payload.data[i] = data[i];
		}
	}

}

uint8_t ConverFrame(frame_t * frame, uint8_t * buffer)
{
	uint8_t i = 0;
	buffer[0] = frame->header.id;
	buffer[1] = frame->header.command;
	buffer[2] = frame->header.lenght;

	for (i = 0; i < frame->header.lenght; i++) {
	        buffer[3 + i] = frame->payload.data[i];
	}

	frame->payload.crc = crc16Calculate(buffer, frame->header.lenght+ 3);
	buffer[3 + frame->header.lenght] = frame->payload.crc >> 8;
	buffer[4 + frame->header.lenght] = frame->payload.crc;
	return 5 + frame->header.lenght;
}

uint16_t crc16Calculate(const uint8_t *buffer, size_t length) {
    uint16_t crc = CRCINITIAL;
    for (size_t i = 0; i < length; i++) {
        crc ^= (buffer[i] << 8);
        for (uint8_t j = 0; j < 8; j++) {
            if (crc & 0x8000) {
                crc = (crc << 1) ^ CRCPOLYNOMIAL;
            } else {
                crc <<= 1;
            }
        }
    }
    return crc;
}
